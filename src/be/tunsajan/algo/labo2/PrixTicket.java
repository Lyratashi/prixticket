/**
 *
 * Contient la methode calculerPrix()
 * @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.algo.labo2;

public class PrixTicket {
    
    
    
    /**
     * 
     * Permet de calculer le prix d un ticket
     * 
     * @param categorie du ticket (doit etre a ou b)
     * @param jour jour de la semaine (1-7)
     * @param age age de la persone (doit etre strictement superieur à 0)
     * @return le prix calculer -1 en cas d erreur
     */
    public static int calculerPrix(char categorie, int jour, int age){
        
        /* Verification requires */
        assert(categorie == 'a' || categorie == 'b') : "Erreur categorie";
        assert (jour > 0 && jour <= 7 ) : "Erreur jour";
        assert(age>0): "Erreur age";
        
        int base;
        if(categorie == 'a') base = 10;
        else base = 20;
        
        switch(jour){
            case 1: /* lundi  - Reduction de 20% -*/ 
                base -= (base / 5);
                break;
            case 2: /* mardi - Pas de spectacle - */
                return -1;
            case 3:
            case 4:
            case 5:
                break;
            case 6:/* Samedi & Dimanche  Reduction de 50%*/ 
            case 7: base+=(base /2);
                break;
            default: return -1; /* ERREUR */   
        }
        if(age < 10 ) return 0; /* Gratuit */
        if(age >= 60) return base - 5;
        return base;
    }
    
}
