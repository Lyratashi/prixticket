/**
 *
 * ALGO: Labo 2: Prix ticket
 *  Permet de calculer le prix d'un ticket
 * en fonction
 * de la categorie
 * du jour de la semaine
 * de l'âge
 * (methode + plan de test)
 * @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.algo.labo2;


public class Test {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        
        /* CONSTANTES */
        final int LUNDI = 1;
        final int MARDI = 2;
        final int MERCREDI = 3;
        final int JEUDI = 4;
        final int VENDREDI = 5;
        final int SAMEDI = 6;
        final int DIMANCHE = 7;
        
        final char A = 'a';
        final char B = 'b';
        /***/
        
        System.out.println("Labo 2: prixTicket()\n Début des test...");
            System.out.println("\t Categorie A:");
                System.out.println("\t\t LUNDI");
                    System.out.print("\t\t\t entre 1 et 9 ans");
                    assert PrixTicket.calculerPrix(A, LUNDI, 1) == 0 : " ...fail";
                    assert PrixTicket.calculerPrix(A, LUNDI, 9) == 0 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t entre 10 et 59 ans");
                    assert PrixTicket.calculerPrix(A, LUNDI, 10) == 8 : " ...fail";
                    assert PrixTicket.calculerPrix(A, LUNDI, 59) == 8 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t 60 ans et +");
                    assert PrixTicket.calculerPrix(A, LUNDI, 60) == 3 : " ...fail";
                    System.out.println(" ...ok ");
                System.out.println("\t\t MARDI");
                    System.out.print("\t\t\t entre 1 et 9 ans");
                    assert PrixTicket.calculerPrix(A, MARDI, 1) == -1 : " ...fail";
                    assert PrixTicket.calculerPrix(A, MARDI, 9) == -1 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t entre 10 et 59 ans");
                    assert PrixTicket.calculerPrix(A, MARDI, 10) == -1 : " ...fail";
                    assert PrixTicket.calculerPrix(A, MARDI, 59) == -1 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t 60 ans et +");
                    assert PrixTicket.calculerPrix(A, MARDI, 60) == -1 : " ...fail";
                    System.out.println(" ...ok ");
                System.out.println("\t\t MERCREDI");
                    System.out.print("\t\t\t entre 1 et 9 ans");
                    assert PrixTicket.calculerPrix(A, MERCREDI, 1) == 0 : " ...fail";
                    assert PrixTicket.calculerPrix(A, MERCREDI, 9) == 0 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t entre 10 et 59 ans");
                    assert PrixTicket.calculerPrix(A, MERCREDI, 10) == 10 : " ...fail";
                    assert PrixTicket.calculerPrix(A, MERCREDI, 59) == 10 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t 60 ans et +");
                    assert PrixTicket.calculerPrix(A, MERCREDI, 60) == 5 : " ...fail";
                    System.out.println(" ...ok ");
                System.out.println("\t\t JEUDI");
                    System.out.print("\t\t\t entre 1 et 9 ans");
                    assert PrixTicket.calculerPrix(A, JEUDI, 1) == 0 : " ...fail";
                    assert PrixTicket.calculerPrix(A, JEUDI, 9) == 0 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t entre 10 et 59 ans");
                    assert PrixTicket.calculerPrix(A, JEUDI, 10) == 10 : " ...fail";
                    assert PrixTicket.calculerPrix(A, JEUDI, 59) == 10 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t 60 ans et +");
                    assert PrixTicket.calculerPrix(A, JEUDI, 60) == 5 : " ...fail";
                    System.out.println(" ...ok ");
                System.out.println("\t\t VENDREDI");
                    System.out.print("\t\t\t entre 1 et 9 ans");
                    assert PrixTicket.calculerPrix(A, VENDREDI, 1) == 0 : " ...fail";
                    assert PrixTicket.calculerPrix(A, VENDREDI, 9) == 0 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t entre 10 et 59 ans");
                    assert PrixTicket.calculerPrix(A, VENDREDI, 10) == 10 : " ...fail";
                    assert PrixTicket.calculerPrix(A, VENDREDI, 59) == 10 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t 60 ans et +");
                    assert PrixTicket.calculerPrix(A, VENDREDI, 60) == 5 : " ...fail";
                    System.out.println(" ...ok ");
                System.out.println("\t\t SAMEDI");
                    System.out.print("\t\t\t entre 1 et 9 ans");
                    assert PrixTicket.calculerPrix(A, SAMEDI, 1) == 0 : " ...fail";
                    assert PrixTicket.calculerPrix(A, SAMEDI, 9) == 0 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t entre 10 et 59 ans");
                    assert PrixTicket.calculerPrix(A, SAMEDI, 10) == 15 : " ...fail";
                    assert PrixTicket.calculerPrix(A, SAMEDI, 59) == 15 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t 60 ans et +");
                    assert PrixTicket.calculerPrix(A, SAMEDI, 60) == 10 : " ...fail";
                    System.out.println(" ...ok ");
                System.out.println("\t\t DIMANCHE");
                    System.out.print("\t\t\t entre 1 et 9 ans");
                    assert PrixTicket.calculerPrix(A, DIMANCHE, 1) == 0 : " ...fail";
                    assert PrixTicket.calculerPrix(A, DIMANCHE, 9) == 0 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t entre 10 et 59 ans");
                    assert PrixTicket.calculerPrix(A, DIMANCHE, 10) == 15 : " ...fail";
                    assert PrixTicket.calculerPrix(A, DIMANCHE, 59) == 15 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t 60 ans et +");
                    assert PrixTicket.calculerPrix(A, DIMANCHE, 60) == 10 : " ...fail";
                    System.out.println(" ...ok ");
            System.out.println("\t Categorie B:");
                System.out.println("\t\t LUNDI");
                    System.out.print("\t\t\t entre 1 et 9 ans");
                    assert PrixTicket.calculerPrix(B, LUNDI, 1) == 0 : " ...fail";
                    assert PrixTicket.calculerPrix(B, LUNDI, 9) == 0 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t entre 10 et 59 ans");
                    assert PrixTicket.calculerPrix(B, LUNDI, 10) == 16 : " ...fail";
                    assert PrixTicket.calculerPrix(B, LUNDI, 59) == 16 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t 60 ans et +");
                    assert PrixTicket.calculerPrix(B, LUNDI, 60) == 11: " ...fail";
                    System.out.println(" ...ok ");
                System.out.println("\t\t MARDI");
                    System.out.print("\t\t\t entre 1 et 9 ans");
                    assert PrixTicket.calculerPrix(B, MARDI, 1) == -1 : " ...fail";
                    assert PrixTicket.calculerPrix(B, MARDI, 9) == -1 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t entre 10 et 59 ans");
                    assert PrixTicket.calculerPrix(B, MARDI, 10) == -1 : " ...fail";
                    assert PrixTicket.calculerPrix(B, MARDI, 59) == -1 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t 60 ans et +");
                    assert PrixTicket.calculerPrix(B, MARDI, 60) == -1 : " ...fail";
                    System.out.println(" ...ok ");
                System.out.println("\t\t MERCREDI");
                    System.out.print("\t\t\t entre 1 et 9 ans");
                    assert PrixTicket.calculerPrix(B, MERCREDI, 1) == 0 : " ...fail";
                    assert PrixTicket.calculerPrix(B, MERCREDI, 9) == 0 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t entre 10 et 59 ans");
                    assert PrixTicket.calculerPrix(B, MERCREDI, 10) == 20 : " ...fail";
                    assert PrixTicket.calculerPrix(B, MERCREDI, 59) == 20 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t 60 ans et +");
                    assert PrixTicket.calculerPrix(B, MERCREDI, 60) == 15 : " ...fail";
                    System.out.println(" ...ok ");
                System.out.println("\t\t JEUDI");
                    System.out.print("\t\t\t entre 1 et 9 ans");
                    assert PrixTicket.calculerPrix(B, JEUDI, 1) == 0 : " ...fail";
                    assert PrixTicket.calculerPrix(B, JEUDI, 9) == 0 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t entre 10 et 59 ans");
                    assert PrixTicket.calculerPrix(B, JEUDI, 10) == 20 : " ...fail";
                    assert PrixTicket.calculerPrix(B, JEUDI, 59) == 20 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t 60 ans et +");
                    assert PrixTicket.calculerPrix(B, JEUDI, 60) == 15 : " ...fail";
                    System.out.println(" ...ok ");
                System.out.println("\t\t VENDREDI");
                    System.out.print("\t\t\t entre 1 et 9 ans");
                    assert PrixTicket.calculerPrix(B, VENDREDI, 1) == 0 : " ...fail";
                    assert PrixTicket.calculerPrix(B, VENDREDI, 9) == 0 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t entre 10 et 59 ans");
                    assert PrixTicket.calculerPrix(B, VENDREDI, 10) == 20 : " ...fail";
                    assert PrixTicket.calculerPrix(B, VENDREDI, 59) == 20 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t 60 ans et +");
                    assert PrixTicket.calculerPrix(B, VENDREDI, 60) == 15 : " ...fail";
                    System.out.println(" ...ok ");
                System.out.println("\t\t SAMEDI");
                    System.out.print("\t\t\t entre 1 et 9 ans");
                    assert PrixTicket.calculerPrix(B, SAMEDI, 1) == 0 : " ...fail";
                    assert PrixTicket.calculerPrix(B, SAMEDI, 9) == 0 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t entre 10 et 59 ans");
                    assert PrixTicket.calculerPrix(B, SAMEDI, 10) == 30 : " ...fail";
                    assert PrixTicket.calculerPrix(B, SAMEDI, 59) == 30 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t 60 ans et +");
                    assert PrixTicket.calculerPrix(B, SAMEDI, 60) == 25 : " ...fail";
                    System.out.println(" ...ok ");
                System.out.println("\t\t DIMANCHE");
                    System.out.print("\t\t\t entre 1 et 9 ans");
                    assert PrixTicket.calculerPrix(B, DIMANCHE, 1) == 0 : " ...fail";
                    assert PrixTicket.calculerPrix(B, DIMANCHE, 9) == 0 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t entre 10 et 59 ans");
                    assert PrixTicket.calculerPrix(B, DIMANCHE, 10) == 30 : " ...fail";
                    assert PrixTicket.calculerPrix(B, DIMANCHE, 59) == 30 : " ...fail";
                    System.out.println(" ...ok");
                    System.out.print("\t\t\t 60 ans et +");
                    assert PrixTicket.calculerPrix(B, DIMANCHE, 60) == 15 : " ...fail";
                    System.out.println(" ...ok ");
       System.out.println("Fin des test...");
    }
    
}
